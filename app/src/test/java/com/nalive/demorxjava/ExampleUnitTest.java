package com.nalive.demorxjava;

import org.junit.Test;

import io.reactivex.Observable;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void testGuGuDan_imperative() {
        int dan = 3;



        Observable.range(1, 9)
                .map(row -> dan + " * " + row + " = " + (dan * row))
                .map(line -> line + '\n')
  //              .subscribe(text -> System.out.print(text))
                .subscribe(System.out::print)
                ;


    }
}