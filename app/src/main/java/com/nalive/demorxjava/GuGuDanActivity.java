package com.nalive.demorxjava;

import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import io.reactivex.Observable;

@EActivity(R.layout.activity_gu_gu_dan)
public class GuGuDanActivity extends AppCompatActivity {

    @ViewById
    EditText edit_dan;

    @ViewById
    TextView text_result;

    @Click(R.id.button_print)
    void clickPrint() {
        text_result.setText("");

        Observable
                .just(edit_dan.getText().toString())

                .map(dan -> Integer.parseInt(dan))
                .filter(dan -> {
                    if (1 < dan && dan < 10) {
                        return true;
                    } else {
                        Toast.makeText(
                                getBaseContext(),
                                "구구단이 허용하지 않는 숫자다:" + dan,
                                Toast.LENGTH_LONG)
                                .show();
                        return false;
                    }
                })
                .flatMap(dan -> Observable.range(1, 9),
                        (dan, row) -> dan + " x " + row + " = " + (dan * row)
                )
                .map(line -> line + "\n")

                .subscribe(text_result::append,
                        e -> Toast.makeText(
                                getBaseContext(),
                                e.toString(),
                                Toast.LENGTH_LONG)
                                .show())
                ;
    }
}
